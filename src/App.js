import logo from './logo.svg';
import './App.css';

import i18n from "i18next";
import { useTranslation, initReactI18next } from "react-i18next";
import { useRef, useState, useEffect } from "react"

const language = localStorage.getItem("language");

i18n
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    // the translations
    // (tip move them in a JSON file and import them,
    // or even better, manage them via a UI: https://react.i18next.com/guides/multiple-translation-files#manage-your-translations-with-a-management-gui)
    resources: {
      en: {
        translation: {
          "title": "Title",
          "submit": "Submit new title"
        }
      },
      ar: {
        translation: {
          "title": "العنوان",
          "submit": "انشر عنوانًا جديدًا"
        }
      }
    },
    lng: language, // if you're using a language detector, do not define the lng option
    fallbackLng: "en",

    interpolation: {
      escapeValue: false // react already safes from xss => https://www.i18next.com/translation-function/interpolation#unescape
    }
  });

function App() {
    const { t } = useTranslation();
    const [titles, setTitles] = useState([]);
    const input = useRef(null);
    useEffect(()=>{
        fetch("http://localhost:1234/titles").then(titles=>titles.json()).then(restit /* Response Title */=>{
            setTitles(restit);
            console.log(restit);
        });
    }, []);
    return (
        <div className="App">
            <button onClick={()=>localStorage.setItem("language", "ar")}>AR</button>
            <button onClick={()=>localStorage.setItem("language", "en")}>EN</button>

            <p> {t('title')} </p>
            <input ref={input} />
            <button onClick={()=>{
                const data = new FormData();
                fetch("http://localhost:1234/titles", {
                    method: "POST",
                    body: input.current.value
                }).then(res=>res.text()).then(console.log);
                setTitles([...titles, input.current.value]);

            }}>{t('submit')}</button>
                <hr />
                <ol>
                {
                titles.map((t, i)=><li key={i}>{t}</li>)
                }
                </ol>
        </div>
    );
}

export default App;
