import express from "express";
import { MongoClient } from 'mongodb'
import bodyParser from "body-parser"
import cors from "cors";

const app = express();
app.use(bodyParser.text())
app.use(cors({}));

const uri = "mongodb://localhost/example?retryWrites=true&w=majority";

const client = new MongoClient(uri);

async function run () {
    try {
        await client.connect();
        const database = client.db('example');
        const titlescollection = database.collection('titles');
        // Query for a movie that has the title 'Back to the Future'
        app.get('/titles', async (_, res) => {
            const title = await titlescollection.find().map(f=>f.title).toArray();
            res.send(JSON.stringify(title));
        })
        app.post('/titles', async (req, res) => {
            console.log(await titlescollection.insertOne({title: req.body}));
            console.log({title: req.body});
            res.send("ok");
        })
        app.listen(1234, () => {
            console.log(`Example app listening on port ${1234}`)
        })
    } finally {
    }
}

run().then().catch(console.error)
